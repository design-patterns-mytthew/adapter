package eu.mytthew;

public class Adapter {
	private MyObject object;

	public Adapter(MyObject myObject) {
		this.object = myObject;
	}

	public void printRow1() {
		object.getList().stream().limit(10).forEach(System.out::println);
	}

	public void printRow2() {
		object.getList().stream().skip(10).limit(10).forEach(System.out::println);
	}

	public void printRow3() {
		object.getList().stream().skip(20).limit(10).forEach(System.out::println);
	}
}
