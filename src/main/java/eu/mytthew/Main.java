package eu.mytthew;

public class Main {
	public static void main(String[] args) {
		MyObject myObject = new MyObject();
		Adapter myNewObject = new Adapter(myObject);
		PrettyPrinter prettyPrinter = new PrettyPrinter(myNewObject);
		prettyPrinter.prettyPrint();
	}
}
