package eu.mytthew;

public class PrettyPrinter {
	private Adapter object;

	public PrettyPrinter(Adapter object) {
		this.object = object;
	}

	public void prettyPrint() {
		object.printRow1();
		object.printRow2();
		object.printRow3();
	}
}
